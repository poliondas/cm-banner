<div class="banner-slide fit-h-">
    <div class="slider">

        <?php
        if ($aBanners) {
            foreach ($aBanners as $banner) {
                $title = ($banner['post_title']);
                $fotos = MkBanner::get_banner_img($banner);
                $itemFoto = isset($banner['mk_banner_item_image']) ? reset($banner['mk_banner_item_image'])['url'] : '';
                $itemFotoClass = isset($banner['mk_banner_item_image_class']) ? $banner['mk_banner_item_image_class'] : '';
                $itemTitulo = isset($banner['mk_banner_item_titulo']) ? $banner['mk_banner_item_titulo'] : '';
                $itemTituloClass = isset($banner['mk_banner_item_titulo_class']) ? $banner['mk_banner_item_titulo_class'] : '';
                $itemTexto = isset($banner['mk_banner_item_texto']) ? $banner['mk_banner_item_texto'] : '';
                $itemTextoClass = isset($banner['mk_banner_item_texto_class']) ? $banner['mk_banner_item_texto_class'] : '';
                $itemBtnTexto = isset($banner['mk_banner_item_btn_texto']) ? $banner['mk_banner_item_btn_texto'] : '';
                $itemBtnTextoLink = isset($banner['mk_banner_item_btn_link']) ? $banner['mk_banner_item_btn_link'] : '';
                $itemBtnTextoClass = isset($banner['mk_banner_item_btn_class']) ? $banner['mk_banner_item_btn_class'] : '';
                $itemPosicao['default'] = !empty($banner['mk_banner_item_posicao']) ? explode('-', $banner['mk_banner_item_posicao'])[0] . ' ' . explode('-', $banner['mk_banner_item_posicao'])[1] : '';
                $itemPosicao['land-lg'] = !empty($banner['mk_banner_item_posicao_l_lg']) ? 'land-lg-' . explode('-', $banner['mk_banner_item_posicao_l_lg'])[0] . ' land-lg-' . explode('-', $banner['mk_banner_item_posicao_l_lg'])[1] : '';
                $itemPosicao['land-xs'] = !empty($banner['mk_banner_item_posicao_l_xs']) ? 'land-xs-' . explode('-', $banner['mk_banner_item_posicao_l_xs'])[0] . ' land-xs-' . explode('-', $banner['mk_banner_item_posicao_l_xs'])[1] : '';
                $itemPosicao['port-lg'] = !empty($banner['mk_banner_item_posicao_p_lg']) ? 'port-lg-' . explode('-', $banner['mk_banner_item_posicao_p_lg'])[0] . ' port-lg-' . explode('-', $banner['mk_banner_item_posicao_p_lg'])[1] : '';
                $itemPosicao['port-xs'] = !empty($banner['mk_banner_item_posicao_p_xs']) ? 'port-xs-' . explode('-', $banner['mk_banner_item_posicao_p_xs'])[0] . ' port-xs-' . explode('-', $banner['mk_banner_item_posicao_p_xs'])[1] : '';
                ?>
                <div class="item item-<?php echo $banner['post_name']; ?> ">
                    <picture class='foto'>
                        <source media="(max-width: 1023px) and (orientation: landscape)" srcset="<?php echo $fotos['mk_banner_foto_l_xs']['medium_large']['url']; ?>">
                        <source media="(min-width: 1024px) and (orientation: landscape)" srcset="<?php echo $fotos['mk_banner_foto_l_lg']['full']['url']; ?>">
                        <source media="(max-width: 600px) and (orientation: portrait)" srcset="<?php echo $fotos['mk_banner_foto_p_xs']['medium']['url']; ?>">
                        <source media="(min-width: 601px) and (orientation: portrait)" srcset="<?php echo $fotos['mk_banner_foto_p_lg']['medium_large']['url']; ?>">
                        <img src="<?php echo $fotos['mk_banner_foto']['full']['url']; ?>">
                    </picture>

                    <?php
                    $hasItem = (!empty($itemFoto) || !empty($itemTitulo) || !empty($itemTexto) || !empty($itemBtnTexto));

                    if ($hasItem) {
                        $stItemPosicao = implode(' ', $itemPosicao);
                        echo "<div class='banner-item {$stItemPosicao} '>";
                        if (!empty($itemFoto)) {
                            echo "<span class='item-foto-wrap {$itemFotoClass}'><img class='item-foto' src='{$itemFoto}'></span>";
                        }
                        if (!empty($itemTitulo)) {
                            $itemTituloLines = explode("\n", $itemTitulo);

                            $i = 1;
                            echo "<div class='title {$itemTituloClass}'>";
                            foreach ($itemTituloLines as $line) {
                                $class = (empty($line)) ? "line-{$i} hidden" : "line-{$i}";
                                echo "<span class='{$class}'>$line</span>";
                                $i++;
                            }
                            echo "</div>";
                        }
                        if (!empty($itemTexto)) {
                            echo "<div class='texto {$itemTextoClass}'>{$itemTexto}</div>";
                        }
                        if (!empty($itemBtnTexto)) {
                            echo "<a class='link {$itemBtnTextoClass}' href='{$itemBtnTextoLink}'>{$itemBtnTexto}</a>";
                        }
                        echo "</div>";
                    }
                    ?>
                </div>
                <?php
            }
        }
        ?>
    </div>
    <?php if (count($aBanners) > 1) { ?>
        <div class="pager"></div>
    <?php }
    ?>
</div>
