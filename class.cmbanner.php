<?php

require_once "post-type-banner.php";

class CmBanner {

    private static $arrSizes = array(
        'cm_banner_foto',
        'cm_banner_foto_l_lg',
        'cm_banner_foto_l_xs',
        'cm_banner_foto_p_lg',
        'cm_banner_foto_p_xs',
    );

    public static function init() {
        # code ...
        add_action( 'admin_init', array('CmBanner', 'check_plugin_dependences' ));
    }

    public static function plugin_activation() {
        # code...
        /*
        if (!is_plugin_active('meta-box/meta-box.php')) {
            
            $message = '
                O plugin CM Banner depende do plugin <a target="_blank" href="http://square.test/wp-admin/plugin-install.php?tab=plugin-information&plugin=meta-box">Meta Box</a>. <br>Instale e ative o plugin <a href="https://br.wordpress.org/plugins/meta-box/">Meta-Box</a> antes de ativar o CM Banner
                ';
            CmBanner::bail_on_activation( $message );
        }*/
    }

    public static function plugin_deactivation() {
        # code ...
    }

    public static function check_plugin_dependences() {
        if (!class_exists( 'CmSite' )) {
            add_action( 'admin_notices', array('CmBanner', 'no_plugin_site'));
            self::desactivate_plugin();
        }
        if (!is_plugin_active('meta-box/meta-box.php')) {
            add_action( 'admin_notices', array('CmBanner', 'no_plugin_metabox'));
            self::desactivate_plugin();
        }
    }

    public static function desactivate_plugin() {
        $plugins = get_option( 'active_plugins' );
        $cmbanner = plugin_basename( 'cm-banner/cm-banner.php' );
        $update  = false;
        foreach ( $plugins as $i => $plugin ) {
            if ( $plugin === $cmbanner ) {
                $plugins[$i] = false;
                $update = true;
            }
        }

        if ( $update ) {
            update_option( 'active_plugins', array_filter( $plugins ) );
        }

        remove_menu_page('edit.php?post_type=cm_banner');
    }

    public static function no_plugin_metabox() {
        ?>
        <div class="error notice">
            <p>
                O plugin CM Banner depende do plugin <a class="thickbox open-plugin-details-modal" href="http://square.test/wp-admin/plugin-install.php?tab=plugin-information&plugin=meta-box&amp;width=772&amp;height=550">Meta Box</a>. <br>Instale e ative o plugin antes de ativar o CM Banner
            </p>
        </div>
        <?php
    }

    public static function no_plugin_site() {
        ?>
        <div class="error notice">
            <p>
                O plugin CM Banner depende do mu-plugin <strong>Site</strong>
            </p>
        </div>
        <?php
    }
    
    public static function add_script_custom() {
        //wp_enqueue_script('cm-cycle', $template_uri . '/assets/lib/cm-cycle2.js', array('jquery'), null, true);
    }

    public static function get_banner_img($banner) {
        $fotos = self::get_banner_img_sizes($banner);
        self::get_banner_img_media($fotos);

        return $fotos;
    }

    public static function get_banner_img_sizes(&$banner) {
        $fotos = array();
        foreach (self::$arrSizes as $sizeBanner) {
            if (isset($banner[$sizeBanner]) && is_array($banner[$sizeBanner])) {
                $foto = reset($banner[$sizeBanner]);
                if (!$foto) {
                    continue;
                }
                if (method_exists('SiteHelpers', 'get_url_img')) {
                    SiteHelpers::get_url_img($foto);
                }
                if (!isset($fotos[$sizeBanner])) {
                    $fotos[$sizeBanner] = array();
                }
                foreach ($foto['sizes'] as $size => $arr) {
                    $fotos[$sizeBanner][$size] = array(
                        'url' => $arr['url'],
                            //'path' => $arr['path'],
                    );
                }
                $fotos[$sizeBanner]['full'] = array(
                    'url' => $foto['url'],
                    'path' => $foto['path'],
                );

                if (!isset($fotos[$sizeBanner]['large'])) {
                    $fotos[$sizeBanner]['large'] = $fotos[$sizeBanner]['full'];
                }
                if (!isset($fotos[$sizeBanner]['medium_large'])) {
                    $fotos[$sizeBanner]['medium_large'] = $fotos[$sizeBanner]['large'];
                }
                if (!isset($fotos[$sizeBanner]['medium'])) {
                    $fotos[$sizeBanner]['medium'] = $fotos[$sizeBanner]['medium_large'];
                }
                if (!isset($fotos[$sizeBanner]['thumbnail'])) {
                    $fotos[$sizeBanner]['thumbnail'] = $fotos[$sizeBanner]['medium'];
                }
            }
        }

        return $fotos;
    }

    public static function get_banner_img_media(&$fotos) {
        if (!isset($fotos['cm_banner_foto_p_xs']) && isset($fotos['cm_banner_foto_p_lg'])) {
            $fotos['cm_banner_foto_p_xs'] = $fotos['cm_banner_foto_p_lg'];
        }
        if (!isset($fotos['cm_banner_foto_l_xs']) && isset($fotos['cm_banner_foto_l_lg'])) {
            $fotos['cm_banner_foto_l_xs'] = $fotos['cm_banner_foto_l_lg'];
        }

        $default = reset($fotos);
        foreach (self::$arrSizes as $size) {
            if (!isset($fotos[$size])) {
                $fotos[$size] = $default;
            }
        }
    }

    public static function deploy($postId = false, $key = 'cm_banner_page', $options = array()) {
        if (empty($key)) {
            $key = 'cm_banner_page';
        }
        $banner_ids = rwmb_meta($key, array('type' => 'select_advanced', 'multiple' => true), $postId);
        $banner_ratio_l = rwmb_meta($key . '_ratio_l', array('type' => 'text'), $postId);
        $banner_ratio_p = rwmb_meta($key . '_ratio_p', array('type' => 'text'), $postId);

        if (isset($options['ratio_l'])) {
            $banner_ratio_l = $options['ratio_l'];
        }
        if (isset($options['ratio_p'])) {
            $banner_ratio_p = $options['ratio_p'];
        }

        if ($banner_ids) {
            global $postTypeBanner;
            $aBanners = $postTypeBanner->find(array('post__in' => $banner_ids));
            $ratioClass = (!empty($banner_ratio_l) || !empty($banner_ratio_l)) ? 'use-ratio' : '';
            echo "<div id='page-banner' class='banner-wrap {$ratioClass}'>";
            if (!empty($banner_ratio_l)) {
                $ratio = explode(',', $banner_ratio_l);
                echo "<img class='ratio ratio_l' src='" . CmBanner::blank_png((int) trim($ratio[0]), (int) trim($ratio[1])) . "'>";
            }
            if (!empty($banner_ratio_p)) {
                $ratio = explode(',', $banner_ratio_p);
                echo "<img class='ratio ratio_p' src='" . CmBanner::blank_png((int) trim($ratio[0]), (int) trim($ratio[1])) . "'>";
            }
            include WP_PLUGIN_DIR . "/cmbanner/banner-html.php";
            echo "</div>";
        }
        wp_reset_postdata();
    }

    public static function metaboxPage($postId = false, &$meta_boxes) {
        if (is_plugin_active('cmbanner/cmbanner.php')) {
            global $postTypeBanner;
            $aBanners = $postTypeBanner->find();

            $options = array();
            foreach ($aBanners as $banner) {
                $options[$banner['id']] = $banner['post_title'];
            }
            $meta_boxes[] = array(
                'title' => 'Banners',
                'priority' => 'high',
                'post_types' => array('page'),
                'fields' => array(
                    array(
                        'name' => 'Banner',
                        'id' => "cm_banner_page",
                        'type' => 'select_advanced',
                        //'type'    => 'select',
                        'multiple' => true,
                        'rows' => 5,
                        'options' => $options,
                    ),
                ),
            );

            wp_reset_postdata();
        }
    }

    public static function blank_png($w = 100, $h = 100) {
        $img = imagecreatetruecolor($w, $h);
// Prepare alpha channel for transparent background
        $alpha_channel = imagecolorallocatealpha($img, 0, 0, 0, 127);
        imagecolortransparent($img, $alpha_channel);
// Fill image
        imagefill($img, 0, 0, $alpha_channel);
// Save transparency
        imagesavealpha($img, true);

        ob_start();
        imagepng($img);
        return 'data:image/png;base64,' . base64_encode(ob_get_clean());
    }

    private static function bail_on_activation( $message, $deactivate = true ) {
        echo "<p>{$message}</p>";
        exit;
    }

}
