<?php

/**
 * @package CmBanner
 */
/*
  Plugin Name: Cm Banner
  Depends: MetaBox
  Plugin URI: https://bitbucket.org/poliondas/cm-banner/overview
  Description: Banners for sites.
  Version: 1.0.0
  Author: Carlos Eduardo Tormina Mateus
  Author URI: https://www.linkedin.com/in/carlos-mateus-52605a9a/
  License: GPLv2 or later
  Text Domain: site
 */

// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define('CMBANNER_PLUGIN_DIR', plugin_dir_path(__FILE__));

register_activation_hook(__FILE__, array('CmBanner', 'plugin_activation'));
register_deactivation_hook(__FILE__, array('CmBanner', 'plugin_deactivation'));

require_once( CMBANNER_PLUGIN_DIR . 'class.cmbanner.php' );

add_action('init', array('CmBanner', 'init'));
add_action('wp_enqueue_scripts', array('CmBanner', 'add_script_custom'));
