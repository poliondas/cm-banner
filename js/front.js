jQuery(function($){

    jQuery('.banner-slide .item .animation-text-implode p').each(function(){
        var $p = $(this);
        var $parent = $p.parent();
        var text = $p.text();
        var aText = text.split('');
        
        for (var i = 0; i < aText.length; i++ ) {
            if( aText[i] === ' ' ) {
                aText[i] = '&nbsp;';
            }
        }
        $p.html( '<span>' +  aText.join('</span><span>') + '</span>');

        initImplodeText( $parent );
    });

    jQuery(".banner-slide .slider").on('cycleStart cycleBefore', function(e,data){
        animateBanner( data );
    });

    jQuery(".banner-slide .slider").each(function(){
        $(this).MKcycle({
            //fx:'slideHorz',
            fx:'fadeBoth',
            slideExp:'.item',
            timeout: 0,
            navigation: true,
            pauseOnHover: true,
            pager: '.pager',
            useDrag: false,
            useKeys: true
        });
    });

    function animateBanner( data ) {
        var $item1 = data.slideNext.find('.item1-outer');
        var $item2 = data.slideNext.find('.item2-outer');

        $item1.removeClass('animating').addClass('prepare-animation');
        $item2.removeClass('animating').addClass('prepare-animation');
        
        setTimeout(function(){
            $item1.addClass('animating').removeClass('prepare-animation');
            $item2.addClass('animating').removeClass('prepare-animation');
        },100);
    }

    function initImplodeText($item) {
        var $spans = $item.find('span');
        var delayIn = parseInt($item.attr('data-animation-delay-in'));
        $spans.each(function(){
            $span = $(this);
            var xx = 4 - Math.floor(Math.random() * 8);
            var yy = 4 - Math.floor(Math.random() * 8);
            var rr = 180 - Math.floor(Math.random() * 360);
            var tt = (delayIn + Math.random()).toFixed(2);

            implodeText($span, xx, yy, rr, tt);
        });
    }

    function implodeText($text, xx, yy, rr, tt) {
        var obj = {
            transform: 'translateX(' + xx + 'em) translateY(' + yy + 'em) rotate(' + rr + 'deg)', 
            opacity: 0,
            'transitionDelay': tt+'s'
        }
        $text.css(obj);
        $text.data('animation-text-implode', obj);
    }
});
